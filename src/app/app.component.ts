import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'recap-front';

  onClick() {
    let Input = ((document.getElementById("text-input") as HTMLInputElement).value)
    let QR = document.getElementById("qr-image") as HTMLImageElement
    
    QR.src = "https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=" + Input
  }
}
